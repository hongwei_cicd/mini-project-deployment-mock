# Mini project deployment mock

This repo mocks a deploy environment with docker compose.

The provided [composefile](./docker-compose.yml) can be used as-is, the only
thing you need to change is the image path for frontend and backend services.

To run the mock deployment, create a `.env` file with content as follows:

```
MARIADB_ROOT_PASSWORD=<password>
```

Where `<password>` is a password of your choice. This file is read as an
environment file from docker compose and the variables defined therein can be
used in our composefile (to avoid committing credentials to git).

Once you're done, and you've pointed the two services to the images in your own
registry, do:

```bash
docker compose up -d
```

And check [this link](https://localhost). You should see something like:

![](./working.png)

If you don't see any text, or you see some other text, something's not working.
